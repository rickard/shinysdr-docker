FROM python:2.7

RUN apt-get update; apt-get install -y \
  \
  \
  cmake \
  libboost-all-dev \
  libsndfile1-dev \
  libcppunit-dev \
  libitpp-dev \
  liblog4cpp5-dev \
  libpulse-dev \
  swig \
  gnuradio-dev \
  gr-osmosdr \
  gr-air-modes \
  ; \
  pip install twisted txws ephem pyserial ; \
  \
    mkdir /build ;  cd /build; \
  git clone https://github.com/argilo/gr-dsd.git; cd /build/gr-dsd; mkdir build; cd build; cmake ..; make; make install; ldconfig \
    ; cd /build; \
  git clone https://github.com/EliasOenal/multimon-ng.git; mkdir -p /build/multimon-ng/build ; cd /build/multimon-ng/build; qmake ../multimon-ng.pro; make; make install \
    ; apt-get remove -y libboost-all-dev swig libpulse-dev; apt-get autoremove; apt-get clean; rm -rf /var/lib/apt/lists; rm -rf /build


RUN mkdir /app; cd /app; git clone https://github.com/kpreid/shinysdr.git; cd /app/shinysdr; /app/shinysdr/fetch-js-deps.sh

CMD cd /app/shinysdr; \
   if [ ! -d /conf/default ]; then PYTHONPATH=/usr/lib/python2.7/dist-packages python -m shinysdr.main --create /conf/default ; fi ; \
   PYTHONPATH=/usr/lib/python2.7/dist-packages python -m shinysdr.main /conf/default
